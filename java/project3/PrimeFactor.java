
import java.util.*;

class PrimeFactor{


    public static void main(String[] args){

	Scanner sc = new Scanner(System.in);
	System.out.println("Enter a number to find the largest prime factor");
	int num = sc.nextInt();
	int res = findPrime(num);
	System.out.println(res);
	

    }

    public static int findPrime(int num){
	
	int result = 2;
	int currDiv = 3;
	int limit = num/2;
	//brute force
	while(currDiv < limit){
	    
	    if(num%currDiv == 0){
		result = currDiv;
	    }
	    currDiv++;
	}
	return result;
    }

}