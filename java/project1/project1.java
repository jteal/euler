//Project Euler - Problem 1
//Jacob Teal
/*
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
*/
import java.util.*;

class NattyNumSum{

    //potential privates go here

    public static void main(String[] args){
	
	//scan in number
	Scanner scan = new Scanner(System.in);
	System.out.println("Please enter x to find the sum of all natural numbers that are multiples of 3 or 5 below x");
	int range  = scan.nextInt();

	int ans = math(range);
	System.out.println("Sum:" + ans);
    }


    public static int math(int x){

	int curr = 1;
	int sum = 0;
	while(curr < x){
	    
	    if (curr % 3 == 0 || curr%5 ==0){
		sum += curr;
	    }
	    curr ++;
	}

	return sum;
    }

}
